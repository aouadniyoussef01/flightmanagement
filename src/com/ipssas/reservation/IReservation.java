package com.ipssas.reservation;

import com.ipssas.flight.Flight;

public interface IReservation {
    void confirm();
    void cancel();
    void setReservationFlight(Flight flight);
}
