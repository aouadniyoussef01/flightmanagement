package com.ipssas.reservation;

import com.ipssas.flight.Flight;

public class InsuranceDecorator extends ReservationDecorator {
    public InsuranceDecorator(Reservation decoratedReservation) {
        super(decoratedReservation);
    }

    @Override
    public void confirm() {
        // Additional logic for confirming with insurance
        super.confirm();
    }

    @Override
    public void cancel() {
        super.cancel();
    }

    @Override
    public void setReservationFlight(Flight flight) {

    }
}