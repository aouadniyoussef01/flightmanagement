package com.ipssas.reservation;

import com.ipssas.enums.ReservationStatus;
import com.ipssas.flight.Flight;

import java.time.LocalDate;
import java.util.Date;

public class Reservation implements IReservation{
    private LocalDate date;
    private int number;
    private Flight flight;
    private ReservationStatus reservationStatus ;
    private boolean hasInsurance;

    public Reservation(LocalDate date, int number, boolean hasInsurance) {
        this.date = date;
        this.number = number;
        this.hasInsurance = hasInsurance;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public ReservationStatus getReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(ReservationStatus reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    public boolean isHasInsurance() {
        return hasInsurance;
    }

    public void setHasInsurance(boolean hasInsurance) {
        this.hasInsurance = hasInsurance;
    }

    @Override
    public void confirm() {
        if (reservationStatus == ReservationStatus.PENDING) {
            // Perform actions to confirm the reservation
            // For example, update the reservation status to CONFIRMED
            reservationStatus = ReservationStatus.CONFIRMED;
            System.out.println("Reservation confirmed.");
        } else {
            System.out.println("Cannot confirm reservation. Status is not pending.");
        }
    }

    @Override
    public void cancel() {
        if (reservationStatus == ReservationStatus.PENDING || reservationStatus == ReservationStatus.CONFIRMED) {
            // Perform actions to cancel the reservation
            // For example, update the reservation status to CANCELED
            reservationStatus = ReservationStatus.CANCELED;
            System.out.println("Reservation canceled.");
        } else {
            System.out.println("Cannot cancel reservation. Status is not pending or confirmed.");
        }
    }

    @Override
    public void setReservationFlight(Flight flight) {
        setFlight(flight);
    }
}

